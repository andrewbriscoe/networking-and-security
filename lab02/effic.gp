#
set title "Efficiency of Data Link Protocols in Stop And Wait Implementations"
set xlabel "Simulated time (seconds)"
set ylabel "Efficency AP/PL"
#
set grid
set yrange [0:]
#
plot	"STOPANDWAIT1.statistics" title "No corruption or packet loss" with linespoints, \
	"STOPANDWAIT2.statistics"	title "Frame Corruption (12.5%)" with linespoints, \
	"STOPANDWAIT3.statistics"	title "Frame Loss (12.5%)" with linespoints, \
	"STOPANDWAIT1-NACK.statistics"	title "No corruption or packet loss (NACK)" with linespoints, \
	"STOPANDWAIT2-NACK.statistics"	title "Frame Corrupt (12.5%) (NACK)" with linespoints, \
	"STOPANDWAIT3-NACK.statistics"	title "Frame Loss (12.5%) (NACK)" with linespoints
#
pause -1
