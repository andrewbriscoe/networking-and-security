#!/bin/sh
#
FREQ=10sec
#
TOPFILE=STOPANDWAIT1
cnet -W -T -e 20m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $TOPFILE.statistics
TOPFILE=STOPANDWAIT1-NACK
cnet -W -T -e 20m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $TOPFILE.statistics

TOPFILE=STOPANDWAIT2
cnet -W -T -e 20m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $TOPFILE.statistics
TOPFILE=STOPANDWAIT2-NACK
cnet -W -T -e 20m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $TOPFILE.statistics


TOPFILE=STOPANDWAIT3
cnet -W -T -e 20m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $TOPFILE.statistics
TOPFILE=STOPANDWAIT3-NACK
cnet -W -T -e 20m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $TOPFILE.statistics

