#!/bin/sh
#
TOPFILE=$1
FREQ=$2 
OUTFILE=$3
#
cnet -W -T -e 5m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $OUTFILE

