#include "checksum_crc16.c"
#include "checksum_ccitt.c"
#include "checksum_internet.c"
#include "corrupt_frame.c"
#include "timing.c"
//#include <sys/time.h>
//#include <time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define FRAMESIZE 100
#define NFRAMES 1000000

int checksum;

void evaluate(unsigned short (*crcFn)(), unsigned short (*corruptFn)(), char *name){
	unsigned char frame[FRAMESIZE];
	int nFailures = 0;
	timing(true);
	printf("%s\n",name);
	printf("-------------\n");
	for (int j=0; j<NFRAMES; j++){
		// POPULATE FRAME WITH RANDOM BYTES
		for (int i=0; i<FRAMESIZE; ++i){
			frame[i] = rand() % 256;
		}
		
		checksum = (*crcFn)(frame, FRAMESIZE);
		corruptFn(frame);
		if (checksum == (*crcFn)(frame,FRAMESIZE)){
			//printf("%s checksum: %d\n",name, checksum);
			//printf("%s checksum: %x\n",name, checksum);
			//printf("FAILED!\n");
			nFailures++;
		}
	}
	printf("number of failures: %d/%d\n",nFailures,NFRAMES);
	printf("%ld usecs\n\n\n",timing(false));
}

int main(void){
	extern void srand(unsigned int seed);

	srand(getpid());
	evaluate((unsigned short (*)()) checksum_crc16, (unsigned short (*)()) corrupt_frame,"CRC16 - Random Switch (Atypical)");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_crc16, (unsigned short (*)()) corrupt_frame2,"CRC16 - Random Byte Complement");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_crc16, (unsigned short (*)()) corrupt_frame3,"CRC16 - Random Bit Flip");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_crc16, (unsigned short (*)()) corrupt_frame4,"CRC16 - Random Burst");


	srand(getpid());
	evaluate((unsigned short (*)()) checksum_ccitt, (unsigned short (*)()) corrupt_frame,"CCITT - Random Switch (Atypical)");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_ccitt, (unsigned short (*)()) corrupt_frame2,"CCITT - Random Byte Complement");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_ccitt, (unsigned short (*)()) corrupt_frame3,"CCITT - Random Bit Flip");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_ccitt, (unsigned short (*)()) corrupt_frame4,"CCITT - Random Burst");
	srand(getpid());

	srand(getpid());
	evaluate((unsigned short (*)()) checksum_internet, (unsigned short (*)()) corrupt_frame,"INTERNET - Random Switch (Atypical)");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_internet, (unsigned short (*)()) corrupt_frame2,"INTERNET - Random Byte Complement");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_internet, (unsigned short (*)()) corrupt_frame3,"INTERNET - Random Bit Flip");
	srand(getpid());
	evaluate((unsigned short (*)()) checksum_internet, (unsigned short (*)()) corrupt_frame4,"INTERNET - Random Burst");

	exit(EXIT_SUCCESS);
}
