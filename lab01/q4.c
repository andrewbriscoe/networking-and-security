#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <endian.h>
//#define Swap8Bytes(val) \
//	 ( (((val) >> 56) & 0x00000000000000FF) | (((val) >> 40) & 0x000000000000FF00) | \
//	   (((val) >> 24) & 0x0000000000FF0000) | (((val) >>  8) & 0x00000000FF000000) | \
//	   (((val) <<  8) & 0x000000FF00000000) | (((val) << 24) & 0x0000FF0000000000) | \
//	   (((val) << 40) & 0x00FF000000000000) | (((val) << 56) & 0xFF00000000000000))


void parseInput(char *out, char *val){
	int i = 0; int j = 15;
	while (i <= 15){
		*(out++) = val[j--];
		i++;
	}
	//for(int i = 0; i <= 100; i+=4){
	//	printf("%d",(int *)(char *) (&line+i));
		//line+=4;
	//}
	//for (int i = 0; i < size; i++){
		//printf("%0x ",Swap8Bytes(*line));
	//}
}

int main(void){
	FILE *file_p;
	file_p = fopen("Q4.output","r");
	if (file_p == NULL){
		printf("Can't open Q4.output");
		exit(EXIT_FAILURE);
	}

	char *line;
	char out;
	while(fread(line,4,1, file_p) != 0){
		parseInput(out,line);
		printf("%d ",out);
	}
	fclose(file_p);
}
