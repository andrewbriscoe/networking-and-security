#!/bin/sh
#
TOPFILE=$1
FREQ=$2 
#
cnet -W -T -e 10m -s -f $FREQ $TOPFILE | grep Efficiency | cut -d: -f 2 | awk '{ printf("%d %s\n", n+=freq, $0); }' freq=$FREQ  > $TOPFILE.statistics

