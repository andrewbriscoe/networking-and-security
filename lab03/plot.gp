#
set title "Efficiency of Piggy backing in Stop And Wait Implementations (No corruption or frame loss)"
set xlabel "Simulated time (seconds)"
set ylabel "Efficency AP/PL"
#
set grid
set yrange [0:]
#
plot	"STOPANDWAIT1.statistics" title "Simple Stop and Wait" with linespoints, \
	"STOPANDWAIT1-PIGGYBACK.statistics"	title "Piggyback stop and wait" with linespoints
#
pause -1
